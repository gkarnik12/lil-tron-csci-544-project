__author__ = 'Akshaya Shanbhogue'

import pickle
import sys

with open(sys.argv[1], 'rb') as ip:
    thesaurusMap = pickle.load(ip)

    with open(sys.argv[2],'wb') as op:
        for (key, val) in thesaurusMap:
            pickle.dump((key, val), op);
