__author__ = 'Akshaya Shanbhogue'

import sys
import argparse
import pickle
import os
import re

regex = re.compile('[^a-zA-Z \'\n]')
# python core/learner/ngram.py compilation/all.txt
# python core/learner/ngram.py core/data/test
class NGram:
    def __init__(self, filename, maxGrams = 4):
        self.filename = filename
        self.maxGrams = maxGrams
        self.p = 2

        lines = ''
        with open(filename) as file_handle:
            lines = file_handle.read()
            lines = regex.sub('', lines)
            lines = lines.lower()
        words = lines.split()
        print('Read ' + str(len(words)) + ' words')

        self.map = {};
        for k in range(0, maxGrams):
            self.map[k] = {}

        for i in range(0,len(words)):
            for k in range(0, maxGrams):
                curr_range = " ".join(words[i-k:i]);
                if curr_range == '' and k!=0:
                    continue
                word = words[i]
                curr_mapEntry = self.map[k].get(curr_range, {})
                self.map[k][curr_range] = curr_mapEntry
                curr_mapVal = curr_mapEntry.get(word, 0)
                curr_mapVal = curr_mapVal + 1
                curr_mapEntry[word] = curr_mapVal

        print('Built models')
        pass

    def getPredictedWords(self, words, vocab, n=10):

        # prenormalized is a multiplicative method.
        # 3 words in a row gets a p^3x boost.
        # 2 words in a row gets a p^3x boost.
        # 1 word in a row gets a px boost.
        preNormalizedMap = {}

        if(len(words) == 0):
            for (entry,val) in self.map[0].get('', {}).items():
                preNormalizedMap[entry] = val
        else:
            for i in range(1, self.maxGrams):
                mapToSearch = self.map[i]
                key = " ".join(words[-i:])
                multiplier = self.p**i;
                curr_range = key;
                tmpMap = mapToSearch.get(curr_range, {})
                for (entry,val) in tmpMap.items():
                    val = val * multiplier
                    # This line boosts existing values linearly. Commenting the line below gives more preference to larger k-grams.
                    # val = preNormalizedMap.get(entry, 0) + val
                    preNormalizedMap[entry] = val

        normalizedList = []
        for (entry, val) in preNormalizedMap.items():
            normalizedList.append({'name':entry, 'probability':val});
        normalizedList.sort(key=lambda x: -x['probability'])
        if len(normalizedList)>10:
            normalizedList=normalizedList[0:10]

        sum = 0
        for entry in normalizedList:
            sum = sum + entry['probability']
        for entry in normalizedList:
            entry['probability']=entry['probability']/sum
        return normalizedList

def main():
    # Example usage
    arguments = argparse.ArgumentParser()
    arguments.add_argument("file")

    args = arguments.parse_args()
    file = args.file
    ng = NGram(file)

    print(ng.getPredictedWords(['a','party'], set()))
    print(ng.getPredictedWords([], set()))

if __name__ == "__main__":
    main()