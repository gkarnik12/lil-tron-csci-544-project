__author__ = 'Nii Mante'
import urllib
import urllib.request
import requests
import argparse
import config
import sys

def create_parser():
    parser = argparse.ArgumentParser()

class NGram:
    def __init__(self, maxGrams = 4):
        self.maxGrams = maxGrams
        pass

    def getPredictedWords(self, words, vocab, n = 10):
        # TODO return best n next predicted words with normalized frequencies from GOOGLE, BING, etc.
        """
        Given an list/empty list of words and a vocabulary, we return a list of
        n predicted words, as well as their log probabilities

        params:
            words -- a list or string of seed words 
            vocab -- a set of words, aka our rap vocabulary
            n -- number of predicted words to return
        returns:
            A dictionary of words as keys, and log probabilities as values
        """

        if n == 0:
            print("Please request more than zero words", file=sys.stderr)
            return []
        #import pdb; pdb.set_trace()
        input_text = words if type(words) is str else ' '.join(words) 
        
        api_keys = config.api_keys()
        params = {
                'u': api_keys['bing_key'], 
                'format' :'json',
                'p': input_text,
                'n': n 
        } 
        results = requests.get(
                'http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12/{}/gen'.format(self.maxGrams),
                params=params
        )
        #import pdb; pdb.set_trace()
        # get the json response from BING api
        predictedJson = results.json()

        # Get the Log probability values and words as two separate Lists
        probabilityValues = predictedJson['probabilities']
        predictedWords = predictedJson['words']

        # Create an array of word, probabilityValue tuples
        wordValTupleList = [(word, val) for word, val in zip(predictedWords,probabilityValues) if word in vocab]
        # wordValDictList = [{ word : val } for word, val in zip(predictedWords, probabilityValues)] 

        return wordValTupleList 

def main():
    """
    How to use the NGram class
    """
    ng = NGram()
    print(ng.getPredictedWords('what is up i', set()), file=sys.stderr)
    print(ng.getPredictedWords('', set()), file=sys.stderr)

if __name__ == "__main__":
    main()

