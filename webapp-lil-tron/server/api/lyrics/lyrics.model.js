'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LyricsSchema = new Schema({
  name: String,
  source: String,
  lyrics:String,
  favorite:Boolean,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('Lyrics', LyricsSchema);
