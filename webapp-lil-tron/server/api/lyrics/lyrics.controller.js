'use strict';

var _ = require('lodash');
var Lyrics = require('./lyrics.model');

// Get list of lyricss
exports.index = function(req, res) {
  Lyrics.find(function (err, lyricss) {
    if(err) { return handleError(res, err); }
    return res.json(200, lyricss);
  });
};

// Get a single lyrics
exports.show = function(req, res) {
  Lyrics.findById(req.params.id, function (err, lyrics) {
    if(err) { return handleError(res, err); }
    if(!lyrics) { return res.send(404); }
    return res.json(lyrics);
  });
};

// Creates a new lyrics in the DB.
exports.create = function(req, res) {
  Lyrics.create(req.body, function(err, lyrics) {
    if(err) { return handleError(res, err); }
    lyrics.source = req.body.source;
    lyrics.favorite = req.body.favorite;
    lyrics.lyrics = req.body.lyrics;
    return res.json(201, lyrics);
  });
};

// Updates an existing lyrics in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Lyrics.findById(req.params.id, function (err, lyrics) {
    if (err) { return handleError(res, err); }
    if(!lyrics) { return res.send(404); }
    var updated = _.merge(lyrics, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, lyrics);
    });
  });
};

// Deletes a lyrics from the DB.
exports.destroy = function(req, res) {
  Lyrics.findById(req.params.id, function (err, lyrics) {
    if(err) { return handleError(res, err); }
    if(!lyrics) { return res.send(404); }
    lyrics.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
