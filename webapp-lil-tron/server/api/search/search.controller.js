'use strict';

var _ = require('lodash');
var Search = require('./search.model');
var http = require('http');
var querystring = require('querystring');
var request = require('request');
var xpath = require('xpath');
var xmldom = require('xmldom').DOMParser;

var postToARapBot;  

var options = {
  host: 'http://localhost:5000',
  path: '/'
};
var siteOptions = {
  "writerbot" : {
    "name" :"writerbot",
    "url" : "http://writerbot.com",
    "path" : "/lyrics",
    "method" : "POST",
    "headers" : {'Content-Type': 'application/x-www-form-urlencoded'}
  },
  
};

exports.search = function(req, res, next){
  var searchText = req.query.query;
  // options.params = req.query.query;

  var externalSourceOptions = siteOptions[req.query.query];
  postToARapBot(externalSourceOptions, req, res, next);
};


postToARapBot = function (options, req, res, next) {
  // Build the post string from an object
  var url = options.url;
  var path = options.path;
  var method = options.method;
  var headers = options.headers;
  var writerBotPostData = function(){
    var wBotEmotionList = ['hap', 'ang', 'dep'];
    var writerBotEmotion = wBotEmotionList[Math.floor(Math.random()*wBotEmotionList.length)];
    var post_data = querystring.stringify({
      'g': 'rbh', 'e': 'ang'
    });
    return post_data;
  };
  var post_data = {
    "writerbot" : writerBotPostData,
  };

  post_data = post_data[options.name];
  post_data = post_data().toString();
  headers['Content-Length'] = Buffer.byteLength(post_data);
  
  

  // An object of options to indicate where to post to
  var post_options = {
      host: url,
      port: '80',
      path: path,
      method: method,
      headers: headers
  };
  var postURL = url+path;
  console.log(postURL);
  console.log(post_data);
  request.post({
      headers : headers,
      url : postURL,
      body : post_data
    },
    function (error, response, body) {
        if (!error && response.statusCode === 200) {
          //var html = response.body;
          //var doc = new xmldom().parseFromString(html);
          //var nodes = xpath.select("//p[@class=lyrics", doc);
          //nodes[0].toString()
          var x = {"source" : options.name, "lyrics" : response.body}
          res.status(200).send(x);
        }else {

          console.log('in error territory');
          console.log(error);
          res.status(500).send(error);
          res.end();
        }
    }
  );

};

// Get list of searchs
exports.index = function(req, res) {
  Search.find(function (err, searchs) {
    if(err) { return handleError(res, err); }
    return res.json(200, searchs);
  });
};

// Get a single search
exports.show = function(req, res) {
  Search.findById(req.params.id, function (err, search) {
    if(err) { return handleError(res, err); }
    if(!search) { return res.send(404); }
    return res.json(search);
  });
};

// Creates a new search in the DB.
exports.create = function(req, res) {
  Search.create(req.body, function(err, search) {
    if(err) { return handleError(res, err); }
    return res.json(201, search);
  });
};

// Updates an existing search in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Search.findById(req.params.id, function (err, search) {
    if (err) { return handleError(res, err); }
    if(!search) { return res.send(404); }
    var updated = _.merge(search, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, search);
    });
  });
};

// Deletes a search from the DB.
exports.destroy = function(req, res) {
  Search.findById(req.params.id, function (err, search) {
    if(err) { return handleError(res, err); }
    if(!search) { return res.send(404); }
    search.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
