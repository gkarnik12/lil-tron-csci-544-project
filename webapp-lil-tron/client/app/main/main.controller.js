'use strict';

angular.module('lilTronApp')
  .controller('MainCtrl', function ($scope, $http, socket) {
    $scope.awesomeThings = [];
    var parser = new DOMParser();

    // $http.get('/api/things').success(function(awesomeThings) {
    //   $scope.awesomeThings = awesomeThings;
    //   socket.syncUpdates('thing', $scope.awesomeThings);
    // });

    
  // ui_search = new UISearch(document.getElementById('sb-search'));
   $scope.lyricCards = [];
   $scope.searchText = '';
   $scope.firstTime = true;
   $scope.favorited = false;
   $scope.showTutorial = false;
   Array.prototype.shuffle = function() {
      var i = this.length, j, temp;
      if ( i === 0 ) {
	 return this;
      }
      while ( --i ) {
         j = Math.floor( Math.random() * ( i + 1 ) );
         temp = this[i];
         this[i] = this[j];
         this[j] = temp;
      }
      return this;
   };
   $scope.getLyrics = function(){
      // Get an id for this lyric

      // Ping the python server with a git request
      var query = window.encodeURIComponent($scope.searchText);
      // 'http://54.149.147.90:5000/search',
      var promise = $http.get(
        'http://localhost:5000/search',
	{params : {query : query}}
      );
      promise.success(function(data){
          var htmlStr = '';
          
          $scope.showTutorial = $scope.firstTime;
          var _id = $scope.lyricCards.length;
          for (var i = 0; i < data.lyrics.length; i++) {
            data.lyrics[i] = data.lyrics[i].charAt(0).toUpperCase() + data.lyrics[i].slice(1);
            htmlStr = htmlStr + data.lyrics[i] + '<br/>';
            
          }
          var expletives = [/fuck/gi, /bitch/gi, /^ass$/gi, /nigga/gi, 
            /^dick$/gi, /nigger/gi];

          var clean = ['f**k', 'b*tch', 'a**', 'ni**a', 
            'd**k', 'n**ger'];
          for(i = 0; i < expletives.length; i++){
            htmlStr = htmlStr.replace(expletives[i], clean[i]);
          }
          $scope.lyricHeader = 'Lyrics';
          $scope.lyricCards.reverse();
          $scope.lyricCards.push({
            '_id': _id, 
            'source' : data.source,
            'lyrics' : htmlStr
          });
          $scope.lyricCards.reverse();
          // $scope.lyricCards.reverse();
      });
      promise.error(function(data){
          //TODO error
          console.log(data);
      });

      var externalSources = ['writerbot'];
      var externalSuccess = function (data){
        var _id = $scope.lyricCards.length;
        var doc = parser.parseFromString(data.lyrics, 'text/html');
        var pObject = doc.querySelectorAll('p.lyrics')[0];
        var truncatedLyrics = getFirstNLines(pObject.innerHTML, 6);
        $scope.lyricCards.reverse();
        $scope.lyricCards.push({
          '_id' : _id,
          'source' : 'writerbot',
          'lyrics' : truncatedLyrics
        });
        $scope.lyricCards.reverse();

      };
      var externalError = function(data){
        //TODO error
        console.log(data);
      };
      for (var i = externalSources.length - 1; i >= 0; i--) {
        $http.get(
          '/api/search/lyrics',
          { params : { query : externalSources[i] }})
        .success(externalSuccess)
        .error(externalError);
      }

    };

    String.prototype.charAtIsUpper = function (atpos){
      var chr = this.charAt(atpos);
      return /[A-Z]|[\u0080-\u024F]/.test(chr) && chr === chr.toUpperCase();
    };

    var getFirstNLines = function(htmlText, numLines){
      var splitElements = htmlText.split(' ');
      var newText = '';
      var numLineBreak = 0;
      for (var i = 0; i < splitElements.length; i++) {
        if (numLineBreak !== numLines){
          if(splitElements[i].indexOf('<br>') !== -1){
            numLineBreak++;
          }
          newText = newText + splitElements[i] + ' ';
          
        }else{
          break;
        }
      }
      return newText;
    };


    $scope.likeLyric = function(lyricCard){
      // Store the liked lyric, as well as the source/url
      // Source could be from other websites or our own api
      // Possibly Create a Source Endpoint
      // Source Model has lyric cards 
      $http.post(
        '/api/source/like',
        { status : 'like', lyric : lyricCard}
      )
      .success(function(data){
        console.log(data);
      })
      .error(function(data){
        console.log(data);
      });
    };
    $scope.dislikeLyric = function(lyricCard){
      $http.post(
        '/api/source/like',
        { status : 'dislike', lyric : lyricCard}
      )
      .success(function(data){
        console.log(data);
      })
      .error(function(data){
        console.log(data);
      });

      var id = lyricCard._id;
      
      if($scope.lyricCards.length === 1){
        $scope.lyricCards = [];
      }else{
        for (var i = $scope.lyricCards.length - 1; i >= 0; i--) {
          if ($scope.lyricCards[i]._id === id){
            $scope.lyricCards.splice(i, 1);
          }
        }
      }
    };

    $scope.favoriteLyric = function(lyricCard){
      $scope.firstTime = false;
      $scope.showTutorial = $scope.firstTime;
      $scope.favorited = true;
      $scope.lyricCards = [];
      $http.post('/api/lyrics', 
        { source : lyricCard.source, favorite : true ,
          lyrics : lyricCard.lyrics
      })
      .success(function(){
        $scope.favorited = false;
      })
      .error(function(){
      });
    };

    $scope.addLyric = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/thing', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing');
    });
  });
