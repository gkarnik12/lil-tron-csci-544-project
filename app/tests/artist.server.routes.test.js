'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Artist = mongoose.model('Artist'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, artist;

/**
 * Artist routes tests
 */
describe('Artist CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Artist
		user.save(function() {
			artist = {
				name: 'Artist Name'
			};

			done();
		});
	});

	it('should be able to save Artist instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Artist
				agent.post('/artists')
					.send(artist)
					.expect(200)
					.end(function(artistSaveErr, artistSaveRes) {
						// Handle Artist save error
						if (artistSaveErr) done(artistSaveErr);

						// Get a list of Artists
						agent.get('/artists')
							.end(function(artistsGetErr, artistsGetRes) {
								// Handle Artist save error
								if (artistsGetErr) done(artistsGetErr);

								// Get Artists list
								var artists = artistsGetRes.body;

								// Set assertions
								(artists[0].user._id).should.equal(userId);
								(artists[0].name).should.match('Artist Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Artist instance if not logged in', function(done) {
		agent.post('/artists')
			.send(artist)
			.expect(401)
			.end(function(artistSaveErr, artistSaveRes) {
				// Call the assertion callback
				done(artistSaveErr);
			});
	});

	it('should not be able to save Artist instance if no name is provided', function(done) {
		// Invalidate name field
		artist.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Artist
				agent.post('/artists')
					.send(artist)
					.expect(400)
					.end(function(artistSaveErr, artistSaveRes) {
						// Set message assertion
						(artistSaveRes.body.message).should.match('Please fill Artist name');
						
						// Handle Artist save error
						done(artistSaveErr);
					});
			});
	});

	it('should be able to update Artist instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Artist
				agent.post('/artists')
					.send(artist)
					.expect(200)
					.end(function(artistSaveErr, artistSaveRes) {
						// Handle Artist save error
						if (artistSaveErr) done(artistSaveErr);

						// Update Artist name
						artist.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Artist
						agent.put('/artists/' + artistSaveRes.body._id)
							.send(artist)
							.expect(200)
							.end(function(artistUpdateErr, artistUpdateRes) {
								// Handle Artist update error
								if (artistUpdateErr) done(artistUpdateErr);

								// Set assertions
								(artistUpdateRes.body._id).should.equal(artistSaveRes.body._id);
								(artistUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Artists if not signed in', function(done) {
		// Create new Artist model instance
		var artistObj = new Artist(artist);

		// Save the Artist
		artistObj.save(function() {
			// Request Artists
			request(app).get('/artists')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Artist if not signed in', function(done) {
		// Create new Artist model instance
		var artistObj = new Artist(artist);

		// Save the Artist
		artistObj.save(function() {
			request(app).get('/artists/' + artistObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', artist.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Artist instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Artist
				agent.post('/artists')
					.send(artist)
					.expect(200)
					.end(function(artistSaveErr, artistSaveRes) {
						// Handle Artist save error
						if (artistSaveErr) done(artistSaveErr);

						// Delete existing Artist
						agent.delete('/artists/' + artistSaveRes.body._id)
							.send(artist)
							.expect(200)
							.end(function(artistDeleteErr, artistDeleteRes) {
								// Handle Artist error error
								if (artistDeleteErr) done(artistDeleteErr);

								// Set assertions
								(artistDeleteRes.body._id).should.equal(artistSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Artist instance if not signed in', function(done) {
		// Set Artist user 
		artist.user = user;

		// Create new Artist model instance
		var artistObj = new Artist(artist);

		// Save the Artist
		artistObj.save(function() {
			// Try deleting Artist
			request(app).delete('/artists/' + artistObj._id)
			.expect(401)
			.end(function(artistDeleteErr, artistDeleteRes) {
				// Set message assertion
				(artistDeleteRes.body.message).should.match('User is not logged in');

				// Handle Artist error error
				done(artistDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Artist.remove().exec();
		done();
	});
});