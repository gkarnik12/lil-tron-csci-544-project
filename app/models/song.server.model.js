'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var VerseSchema = new Schema({
	content: String,
	explanation: String
});

var SectionSchema = new Schema({
	name: String,
	verses: [VerseSchema]
});

/**
 * Song Schema
 */
var SongSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Song name',
		trim: true
	},

	mainArtist: {
		type: Schema.ObjectId,
		ref: 'Artist'
	},
	featuringArtists: [{
		type: Schema.ObjectId,
		ref: 'Artist'
	}],
	producingArtists: [{
		type: Schema.ObjectId,
		ref: 'Artist'
	}],
	sections: [SectionSchema],


	link: String,
	created: {
		type: Date,
		default: Date.now
	}
});

SongSchema.index({ link: 1, mainArtist: 1 }, { unique: true });

mongoose.model('Song', SongSchema);
