'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Artist Schema
 */
var ArtistSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Artist name',
		trim: true,
		unique: true
	},
	link: String,
	popularSongs:[String],
	songs:[String],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

var findOrCreate = require('mongoose-findorcreate');
ArtistSchema.plugin(findOrCreate);

mongoose.model('Artist', ArtistSchema);
