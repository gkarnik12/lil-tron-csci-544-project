'use strict';
var rapgeniusClient = require("rapgenius-js");

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Artist = mongoose.model('Artist'),
	Song = mongoose.model('Song'),
	_ = require('lodash');

/**
 * Create a Artist
 */
exports.create = function(req, res) {
	var artist = new Artist(req.body);
	artist.user = req.user;

	artist.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(artist);
		}
	});
};

/**
 * Show the current Artist
 */
exports.read = function(req, res) {
	res.jsonp(req.artist);
};

/**
 * Update a Artist
 */
exports.update = function(req, res) {
	var artist = req.artist ;

	artist = _.extend(artist , req.body);

	artist.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(artist);
		}
	});
};

/**
 * Delete an Artist
 */
exports.delete = function(req, res) {
	var artist = req.artist ;

	artist.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(artist);
		}
	});
};

/**
 * List of Artists
 */
exports.list = function(req, res) { 
	Artist.find().sort('-created').populate('user', 'displayName').exec(function(err, artists) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(artists);
		}
	});
};

/**
 * Artist middleware
 */
exports.artistByID = function(req, res, next, id) { 
	Artist.findById(id).populate('user', 'displayName').exec(function(err, artist) {
		if (err) return next(err);
		if (! artist) return next(new Error('Failed to load Artist ' + id));
		req.artist = artist ;
		next();
	});
};

/**
 * Artist authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.artist.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
exports.populateFromRapGenius = function(req, res){
	res.json({
		success: true
	});
}

var lyricsSearchCb = function(artistFound, link){
	return function(err, lyricsAndExplanations){
		if(err){
			console.log("Error: " + err);
		}else{
			//Printing lyrics with section names
			var lyrics = lyricsAndExplanations.lyrics;
			var explanations = lyricsAndExplanations.explanations;

			//Now we can embed the explanations within the verses
			lyrics.addExplanations(explanations);
			var firstVerses = lyrics.sections[0].verses[0];
			var song = new Song({
				name:lyrics.songTitle,
				mainArtist:artistFound,
				link: link,
				sections:lyrics.sections
				//name:lyrics.featuringArtists,
				//name:lyrics.producingArtists,
			});

			song.save(function(err){
				console.log(err);
			});
		}
	};

}
function createFreshSong(name, artist, link){
	Song.findOne({mainArtist:artist, link:link}, function(err, found){
		if(!found)
			rapgeniusClient.searchLyricsAndExplanations(link, "rap", lyricsSearchCb(artist, link));
		else console.log("already found "+found.name);
	});
}
exports.populateFromRG = function(req, res, next, id){
	Artist.findOrCreate({
		name: id
	}, function(err, artistFound, created) {
		rapgeniusClient.searchArtist(id, "rap", function (err, artist) {
			if (err) {
				console.log("Error: " + err);
			} else {
				console.log("Rap artist found [name=%s, link=%s, popular-songs=%d]",
					artist.name, artist.link, artist.popularSongs.length, artist.songs.length);
				// add to DB
				for (var i=0;i<artist.songs.length;i++) {
					var song = artist.songs[i];
					createFreshSong(song.name, artistFound, song.link);
				}
			}
			next();
		});
	});


}


var fs = require('fs');

var writeArtistToFiles = function(artist){
	Song.find({mainArtist:artist}).exec(function(err, songs){
		if(!err && songs)
			for(var i = 0;i<songs.length;i++){
				var song = songs[i];
				var lines = ""
				if(song.sections)
				for(var j =0;j<song.sections.length;j++){
					var section = song.sections[j]
					if(section.verses)
					for(var k =0;k<section.verses.length;k++){
						lines = lines + "\n" + section.verses[k].content
					}
				}
				song.name = song.name.replace(/[*:'\\\/"?]/g, '');

				fs.writeFileSync("data/"+artist.name+"/"+song.name+".txt", lines);
				console.log("data/"+artist.name+"/"+song.name+".txt");
			}
	});
}

exports.dumpAll = function(req, res){
	Artist.find().exec(function(err, artists){
		if(!err && artists)
			for(var i = 0;i<artists.length;i++) {
				var artist = artists[i];
				var dir = "data/" + artist.name;
				if (!fs.existsSync(dir)) {
					fs.mkdirSync(dir);
				}
				writeArtistToFiles(artist);
			}
	});
	res.status(200).send();
}

