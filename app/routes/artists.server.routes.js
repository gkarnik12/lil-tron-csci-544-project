'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var artists = require('../../app/controllers/artists.server.controller');

	// Artists Routes
	app.route('/artists')
		.get(artists.list)
		.post(users.requiresLogin, artists.create);

	app.route('/artists/:artistId')
		.get(artists.read)
		.put(users.requiresLogin, artists.hasAuthorization, artists.update)
		.delete(users.requiresLogin, artists.hasAuthorization, artists.delete);


	app.route('/artists/populate/:rgArtistName')
		.get(artists.populateFromRapGenius);
	// Finish by binding the Artist middleware

	app.route('/artists/dump/s').get(artists.dumpAll);

	app.param('artistId', artists.artistByID);
	app.param('rgArtistName', artists.populateFromRG);
};
