__author__ = 'Gandhali Karnik'
import pickle
import nltk
import sys
import argparse
import json
import random
import ngram
import time



posMap = {
    "CD":"noun",
    "NN":"noun",
    "NNS":"noun",
    "NNP":"noun",
    "NNPS":"noun",
    "VB":"verb",
    "VBD":"verb",
    "VBG":"verb",
    "VBN":"verb",
    "VBP":"verb",
    "VBZ":"verb",
    "RB":"adverb",
    "RBR":"adverb",
    "RBS":"adverb",
    "JJ":"adjective",
    "JJR":"adjective",
    "JJS":"adjective"
}

# python3 core/generator/lilTron.py data/all.txt data/json_updated_related_words data/json_rhymes_normalized
class LilTron:
    def __init__(self, rapNGram, externalNGram, rhymeMap, thesaurusMap, vocab, posMap):
        self.rapNGram = rapNGram
        self.externalNGram = externalNGram
        self.rhymeMap = rhymeMap
        self.thesaurusMap = thesaurusMap
        self.vocab = vocab
        self.posMap = posMap
        pass

    def generateLine(self, line):
        # TODO return line
        # use self.rapNGram.getPredictedWords(words, self.vocab)
        words = line.split()
        templine = line
        newLine = []
        lineLength = 18 #random.randint(7,9) 
        # pos flag is true if the last word is not a noun adj or verb 
        posFlag = True
        while len(newLine) < lineLength:
            new_words = None
            if random.uniform(0,1) < .9:
                new_words = self.rapNGram.getPredictedWords(words, set())
            else:
                new_words = self.externalNGram.getPredictedWords(words, self.vocab)
                if len(new_words) == 0:
                    new_words = self.rapNGram.getPredictedWords(words, set())
            new_word = self.weighted_choice(new_words)
            newLine.append(new_word)
            words.append(new_word)
            tagged_words = nltk.pos_tag(words)
            if tagged_words[-1][1] in posMap:
                postag = posMap[tagged_words[len(tagged_words)-1][1]]
                if postag == "noun" or postag == "adjective" or postag == "verb":
                    posFlag = False
                else:
                    posFlag = True
            else:
                posFlag = True
        line = ' '.join(newLine)
         
        rhymeLine = self.rhymify(line)
        if rhymeLine == line:
            return self.generateLine(templine)
        return [rhymeLine,line]
         
        #x = self.rhymify(line)
        #return [x,line]
        pass

    def rhymify(self, line):
        # use self.rhymeMap and self.thesaurusMap to get optimal split
        words = line.split(' ')
        tagged_words = nltk.pos_tag(words)
        window = int(len(words)/2)-2
        index1 = -1 
        new_word1 = ""
        new_word2 = ""
        reduction_factor = 0.3
        max_likelihood = 0
        shift = 0
        last_synonym_list = []
        changed = False
        
        if tagged_words[len(tagged_words)-1][1] in posMap:
            postag = posMap[tagged_words[len(tagged_words)-1][1]]
            if words[len(words)-1] in self.thesaurusMap and postag in self.thesaurusMap[words[len(words)-1]] and 'syn' in self.thesaurusMap[words[len(words)-1]][postag]:
                last_synonym_list = self.thesaurusMap[words[len(words)-1]][postag]['syn']
        last_synonym_list.append(words[len(words)-1])

        while shift <= 6:
            synonym_list = []
            if tagged_words[window][1] in posMap:
                postag = posMap[tagged_words[window][1]]
                if words[window] in self.thesaurusMap and postag in self.thesaurusMap[words[window]] and 'syn' in self.thesaurusMap[words[window]][postag]: 
                    synonym_list = self.thesaurusMap[words[window]][postag]['syn']
            synonym_list.append(words[window])
            for syn in synonym_list:
                for last_syn in last_synonym_list:
                    syn_list = syn.split()
                    last_syn_list = last_syn.split()
                    l = self.likelihood(syn_list[len(syn_list)-1], last_syn_list[len(last_syn_list)-1], reduction_factor)
                    if l > max_likelihood:
                        max_likelihood = l
                        index1 = window
                        new_word1 = syn
                        new_word2 = last_syn
                        changed = True
            #import pdb; pdb.set_trace()
            shift+=1
            window += 1
        if changed:
            words[index1]=new_word1 
            words[len(words)-1]=new_word2
            words = words[:index1+1] + ["<br/>"] + words[index1+1:]
        # Final modified sentence
        return ' '.join(words)

    def likelihood(self, w1, w2, reduction_factor):
        # factor = reduction_factor * rhyme_score * synonym_score
        if w1 in self.rhymeMap:
            if w2 in self.rhymeMap[w1]:
                return self.rhymeMap[w1][w2]
        if w2 in self.rhymeMap:
            if w1 in self.rhymeMap[w2]:
                return self.rhymeMap[w2][w1]
        return 0

    def weighted_choice(self, choices):
        r = random.uniform(0, 1)
        upto = 0
        for w in choices:
            if upto + w['probability'] >= r:
                return w['name']
            upto += w['probability']

def main():
    arguments = argparse.ArgumentParser()
    arguments.add_argument("file")
    arguments.add_argument("related_words")
    arguments.add_argument("rhymes_normalized")
    thesaurusMap = dict()
    rhymeMap = dict()

    args = arguments.parse_args()
    file = args.file
    related_words = args.related_words
    rhymes_normalized = args.rhymes_normalized

    with open(rhymes_normalized, 'r') as h:
        rhymeList = json.load(h)

    print("Loading thesaurus....")
    with open(related_words, 'r') as h:
        thesaurusMap = json.load(h)

    print("Loading rhymes....")
    with open(rhymes_normalized, 'r') as h:
        rhymeList = json.load(h)
    for rhymekey in rhymeList:
        rhymeMap[rhymekey] = dict()
        for value in rhymeList[rhymekey]:
            rhymeMap[rhymekey][value['word']] = value['score']

    print("Loading ngram....")
    ng = ngram.NGram(file)

    lilObj = LilTron(ng, None, rhymeMap, thesaurusMap, None, posMap)
    line = ""

    for i in range(0,8):
        newLine = lilObj.generateLine(line)
        print(newLine[0])
        line = newLine[1]

if __name__ == "__main__":
    main()
