# Lil Tron Python server

	usage: python3 lilTronPythonServer.py


This code starts a `Flask` python server, and invokes the 
lilTron.py module. The `lilTron.py` module relies on:

- `ngram.py`
- `externalngram.py`
- files in the `data/` directory 

This service starts on port 5000 and waits for `GET` requests
from our web-client.  For each `GET` request, we return a 
set of generated lyrics to the client.
