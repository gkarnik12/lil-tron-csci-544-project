__author__ = 'Akshaya Shanbhogue'

import pickle
import sys
import json

with open(sys.argv[1], 'r') as ip:
    rhymeMap = json.load(ip)

    with open(sys.argv[2],'wb') as op:
        for (key, val) in rhymeMap.items():
            pickle.dump((key, val), op);
