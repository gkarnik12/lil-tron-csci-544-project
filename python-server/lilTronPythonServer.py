from flask import Flask, request
from flask.ext.cors import CORS
import argparse
import sys
import lilTron
import json
import ngram
import externalngram
import urllib.parse
import random
from pickle import UnpicklingError
import pickle
from pympler import muppy 
from pympler import summary
from pympler import tracker
#from crossdomain import crossdomain

# thesaurusMap, rhymeMap = "", ""
lilTronInstance = None
 
class ArgsDict(dict):
    pass
def create_app():
    _app = Flask(__name__)
    arguments = argparse.ArgumentParser()
    arguments.add_argument(
        "-l",
        "--lyrics_file",
        help="A filename path containing all of the tokens as a text file."
        )
    arguments.add_argument(
        "-t",
        "--thesaurus",
        type=argparse.FileType('r'),
        help="A JSON file containing all of the related words"
        )
    arguments.add_argument(
        "-r",
        "--rhymes_normalized",
        type=argparse.FileType('r'),
        help='A JSON file containing all of the rhyming words and their normalized scores'
        )

    #args = arguments.parse_args()
    
    
    ng, rhymeMap, thesaurusMap, vocab, posMap = None, None, None, None, None
    if not sys.flags.debug:
        args = ArgsDict() 
        args.lyrics_file = 'data/all.txt'
        args.rhymes_normalized = 'data/stream_rhymes_normalized'
        args.thesaurus = 'data/stream_updated_related_words'
        args.vocab = 'data/vocab'
        print("Loading vocab....", file=sys.stderr)
        with open(args.vocab, 'rb') as v:
            vocab = pickle.load(v)
        print("Loading ngram....", file=sys.stderr)
        ng = ngram.NGram(args.lyrics_file)
        externalng = externalngram.NGram()

        print(_app.default_config['SERVER_NAME'], file=sys.stderr)
        print("Loading thesaurus....", file=sys.stderr)
        #thesaurusMap = json.load(args.thesaurus)
        thesaurusMap = {}
        with open(args.thesaurus, 'rb') as h:
            while True:
                try:
                    (key, val) = pickle.load(h)
                    thesaurusMap[key] = val
                except (EOFError, UnpicklingError):
                    print('Reached end of thesaurus file', file=sys.stderr)
                    break
        
        print("Loading rhymes....", file=sys.stderr)
        rhymeMap = {} 
        with open(args.rhymes_normalized, 'rb') as l:
            while True:
                try:
                    (rhymeKey, keyList) = pickle.load(l)
                    rhymeMap[rhymeKey] = { value['word'] : value['score'] for value in keyList }
                except (EOFError, UnpicklingError):
                    print('Reached end of rhymes file', file=sys.stderr)
                    break

        posMap = lilTron.posMap

    return _app, ng, externalng, rhymeMap,thesaurusMap, vocab, posMap

# initialize the server as well as some lilTron variables
app, ng, externalng, rhymeMap, thesaurusMap, vocab, posMap = create_app()

# Start lil tron if we're in not debug mode
if not sys.flags.debug:
    lilTronInstance = lilTron.LilTron(ng, externalng, rhymeMap, thesaurusMap, vocab, posMap)
    #lilTronInstance = lilTronMongo.LilTronMongo(ng, None, None, posMap)

cors = CORS(app, resources= { r'/search/*' : {"origins" : "*"} }, allow_headers='Content-Type')


@app.route("/search", methods=["GET","OPTIONS"])
def run_tron():
    # Receive request from angular client
    # Consists of a query string with seed lyrics or it's empty
    print('Starting request', file=sys.stderr)

    lyrics = [] 
    if sys.flags.debug:
        # Use this as quick test since Lang Model takes time to build at first
        # Create random shuffled lyrics for the client
        lyrics = ['yo what up', 'im a boss', 'i do work', 'you cant check this']
        randIndex = random.randint(0,len(lyrics)-1)
        lyrics = lyrics[randIndex:]
        random.shuffle(lyrics)
    else:
        # Generate 8 lines of lyrics
        line = request.query_string
        line = urllib.parse.parse_qs(line)
        line = '' if len(line) < 1  else urllib.parse.unquote(str(line[b'query'][0])[2:-1])
	
        for i in range(4): 
            newLine = lilTronInstance.generateLine(line)
            # lyrics.append(tempLine + ' '.join(newLine[0].split()[2:-2]))
            lyrics.append(newLine[0])
            line = newLine[1]
            
    # Use this to distinguish where lyrics came from, we'll use other websites
    # such as writebot or rap bot to grab lyrics 
    # This will purely be for evaluation purposes, users will be able to like/pick
    # their favorite lyrics out of a set ours, and other sources
    source = "lil-tron"
    response = json.dumps({ 'headers' : { 'Access-Control-Allow-Origin' : '*' }, "source" : source, 'lyrics' : lyrics })

    return response 

if __name__ == "__main__":
    random.seed()
    
    app.run(host='0.0.0.0')

