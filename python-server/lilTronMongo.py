__author__ = 'Nii Mante'

import pickle
import nltk
import sys
import argparse
import json
import random
import ngram
import pymongo 
from bson.json_util import dumps
import time


posMap = {
    "CD":"noun",
    "NN":"noun",
    "NNS":"noun",
    "NNP":"noun",
    "NNPS":"noun",
    "VB":"verb",
    "VBD":"verb",
    "VBG":"verb",
    "VBN":"verb",
    "VBP":"verb",
    "VBZ":"verb",
    "RB":"adverb",
    "RBR":"adverb",
    "RBS":"adverb",
    "JJ":"adjective",
    "JJR":"adjective",
    "JJS":"adjective"
}

# python3 core/generator/lilTron.py data/all.txt data/json_updated_related_words data/json_rhymes_normalized
class LilTronMongo:
    def __init__(self, rapNGram, externalNGram, vocab, posMap):
        self.rapNGram = rapNGram
        self.externalNGram = externalNGram
        self.vocab = vocab
        self.rhymeMap = None 
        self.thesaurusMap = None 
        self.posMap = posMap
        self.db = pymongo.MongoClient().liltron_data
        self.initMapsFromDB()
        pass

    def initMapsFromDB(self):
        t0 = time.time()
        print('Initializing thesaurus map from DB', file=sys.stderr)
        self.thesaurusMap= self.pymongoCursorToDict(self.db.thesaurus.find())
        print('Initializing rhymes map from DB', file=sys.stderr)
        self.rhymeMap = self.pymongoCursorToDict(self.db.rhymes.find()) 
        t1 = time.time()
        print('Initialization took {} seconds'.format(str(t1-t0)), file=sys.stderr)

    def generateLine(self, line):
        # TODO return line
        # use self.rapNGram.getPredictedWords(words, self.vocab)
        words = line.split()
        newLine = []
        while len(newLine)<14:
            new_words = self.rapNGram.getPredictedWords(words, set())
            new_word = self.weighted_choice(new_words)
            newLine.append(new_word)
            words.append(new_word)
        line = ' '.join(newLine)
        return [self.rhymify(line),line]
        pass

    def pymongoCursorToDict(self, cursorObj):
        objString = dumps(cursorObj)
        if objString == '[]':
            return None
        if objString[0] == "[":
            objString = objString[1:-1]
        jsonObj = json.loads(objString)
        return jsonObj

    def rhymify(self, line):
        # use self.rhymeMap and self.thesaurusMap to get optimal split
        words = line.split(' ')
        tagged_words = nltk.pos_tag(words)
        window = int(len(words)/2)-3
        index1 = 0
        new_word1 = ""
        new_word2 = ""
        reduction_factor = 0.3
        max_likelihood = 0
        shift = 0
        last_synonym_list = []
        changed = False
        
        if tagged_words[len(tagged_words)-1][1] in posMap:
            postag = posMap[tagged_words[-1][1]]
            curr_word = words[-1]
            #curr_word_obj = self.pymongoCursorToDict(self.db.thesaurus.find({'word' : curr_word }))
            curr_word_obj = self.thesaurusMap[curr_word]
            if curr_word_obj is not None and postag in curr_word_obj['related'] and 'syn' in curr_word_obj['related'][postag]:
                last_synonym_list = curr_word_obj['related'][postag]['syn']
        last_synonym_list.append(words[-1])

        while shift <= 6:
            synonym_list = []
            if tagged_words[window][1] in posMap:
                postag = posMap[tagged_words[window][1]]
                #thesaurusCursorObj = self.db.thesaurus.find({'word' : words[window]})
                #curr_window_word_obj = self.pymongoCursorToDict(thesaurusCursorObj)
                curr_window_word_obj = self.thesaurusMap[words[window]]
                if curr_window_word_obj is not None and postag in curr_window_word_obj['related'] and 'syn' in curr_window_word_obj['related'][postag]:
                    synonym_list = curr_window_word_obj['related'][postag]['syn']
            synonym_list.append(words[window])

            for syn in synonym_list:
                for last_syn in last_synonym_list:
                    syn_list = syn.split()
                    last_syn_list = last_syn.split()
                    l = self.likelihood(syn_list[-1], last_syn_list[-1], reduction_factor)
                    if l > max_likelihood:
                        max_likelihood = l
                        index1 = window
                        new_word1 = syn
                        new_word2 = last_syn
                        changed = True
            shift+=1
            window += 1
        
        if changed:
            words[index1]=new_word1
            words[-1]=new_word2

        # Final modified sentence
        return ' '.join(words)

    def likelihood(self, w1, w2, reduction_factor):
        # factor = reduction_factor * rhyme_score * synonym_score
        #w1_object = self.pymongoCursorToDict(self.db.rhymes.find({'word' : w1})) 
        w1_object = self.rhymeMap[w1]
        if w1_object is not None:
            if w2 in w1_object['rhymes']:
                return w1_object['rhymes'][w2]
        #w2_object = self.pymongoCursorToDict(self.db.rhymes.find({'word' : w2})) 
        w2_object = self.rhymeMap[w2]
        if w2_object is not None:
            if w1 in w2_object['rhymes']:
                return w2_object['rhymes'][w1]
        return 0

    def weighted_choice(self, choices):
        r = random.uniform(0, 1)
        upto = 0
        for w in choices:
            if upto + w['probability'] >= r:
                return w['name']
            upto += w['probability']

def main():
    arguments = argparse.ArgumentParser()
    arguments.add_argument("file")

    args = arguments.parse_args()
    file = args.file
    
    print("Loading ngram....")
    ng = ngram.NGram(file)

    lilObj = LilTronMongo(ng, None, None, posMap)
    line = ""

    for i in range(0,8):
        newLine = lilObj.generateLine(line)
        print(newLine[0])
        line = newLine[1]

if __name__ == "__main__":
    main()
