'use strict';

//Setting up route
angular.module('artists').config(['$stateProvider',
	function($stateProvider) {
		// Artists state routing
		$stateProvider.
		state('listArtists', {
			url: '/artists',
			templateUrl: 'modules/artists/views/list-artists.client.view.html'
		}).
		state('createArtist', {
			url: '/artists/create',
			templateUrl: 'modules/artists/views/create-artist.client.view.html'
		}).
		state('viewArtist', {
			url: '/artists/:artistId',
			templateUrl: 'modules/artists/views/view-artist.client.view.html'
		}).
		state('editArtist', {
			url: '/artists/:artistId/edit',
			templateUrl: 'modules/artists/views/edit-artist.client.view.html'
		});
	}
]);