'use strict';

// Configuring the Articles module
angular.module('artists').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Artists', 'artists', 'dropdown', '/artists(/create)?');
		Menus.addSubMenuItem('topbar', 'artists', 'List Artists', 'artists');
		Menus.addSubMenuItem('topbar', 'artists', 'New Artist', 'artists/create');
	}
]);