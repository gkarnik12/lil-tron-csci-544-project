'use strict';

// Artists controller
angular.module('artists').controller('ArtistsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Artists',
	function($scope, $stateParams, $location, Authentication, Artists) {
		$scope.authentication = Authentication;

		// Create new Artist
		$scope.create = function() {
			// Create new Artist object
			var artist = new Artists ({
				name: this.name
			});

			// Redirect after save
			artist.$save(function(response) {
				$location.path('artists/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Artist
		$scope.remove = function(artist) {
			if ( artist ) { 
				artist.$remove();

				for (var i in $scope.artists) {
					if ($scope.artists [i] === artist) {
						$scope.artists.splice(i, 1);
					}
				}
			} else {
				$scope.artist.$remove(function() {
					$location.path('artists');
				});
			}
		};

		// Update existing Artist
		$scope.update = function() {
			var artist = $scope.artist;

			artist.$update(function() {
				$location.path('artists/' + artist._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Artists
		$scope.find = function() {
			$scope.artists = Artists.query();
		};

		// Find existing Artist
		$scope.findOne = function() {
			$scope.artist = Artists.get({ 
				artistId: $stateParams.artistId
			});
		};
	}
]);