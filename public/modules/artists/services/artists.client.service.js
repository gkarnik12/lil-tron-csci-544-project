'use strict';

//Artists service used to communicate Artists REST endpoints
angular.module('artists').factory('Artists', ['$resource',
	function($resource) {
		return $resource('artists/:artistId', { artistId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);