__author__ = 'Akshaya Shanbhogue'

import sys
import argparse
import pickle
import os
import re
regex=re.compile('[^a-zA-Z \']')
vocab = set()

if __name__ == "__main__":
    # python python/formatData.py data compilation vocab
    arguments = argparse.ArgumentParser()
    arguments.add_argument("dir_from")
    arguments.add_argument("dir_to")
    arguments.add_argument("vocab")

    args = arguments.parse_args()
    dir_from = args.dir_from
    dir_to = args.dir_to

    with open(dir_to+"/all.txt", 'w', errors='ignore') as write_all_handle:
        for dir in os.listdir(dir_from):
            print(dir)
            with open(dir_to+"/"+dir+".txt", 'w', errors='ignore') as write_handle:
                for file in os.listdir(dir_from+"/"+dir):
                    try:
                        with open(dir_from+"/"+dir+"/"+file, 'r', errors='ignore') as read_handle:
                            lines = read_handle.read().splitlines()
                            for line in lines:
                                line = regex.sub('', line)
                                if len(line)>0:
                                    line = line.lower()
                                    write_handle.write(line+'\n')
                                    write_all_handle.write(line+'\n')
                                    for word in line.split():
                                        vocab.add(word)
                    except:
                        pass

    with open(args.vocab, "wb") as out:
        pickle.dump(vocab, out)