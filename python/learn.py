__author__ = 'Akshaya Shanbhogue'
import nltk
import argparse
import pickle
import re
import random
from nltk.collocations import *

regex=re.compile('[^a-zA-Z \'\n]')
quadgram_measures = nltk.metrics.association.QuadgramAssocMeasures()

if __name__ == "__main__":
    # python python/learn.py compilation/all.txt vocab
    arguments = argparse.ArgumentParser()
    arguments.add_argument("training_file")
    arguments.add_argument("vocab")

    args = arguments.parse_args()
    training = args.training_file
    vocab_file = args.vocab

    with open(vocab_file, "rb") as file:
        vocab = pickle.load(file)

    with open(training, 'r', errors='ignore') as read_handle:
        lines = read_handle.read()
        lines = regex.sub('', lines)
        lines = lines.lower()
    # words = nltk.word_tokenize(lines)
    words = lines.split()
    print("read "+str(len(words))+" words")
    # my_bigrams = nltk.bigrams(words)
    # my_trigrams = nltk.trigrams(words)

    # http://stackoverflow.com/questions/17531684/n-grams-in-python-four-five-six-grams
    # Look at collocations, http://www.nltk.org/howto/collocations.html
    # from nltk.collocations import *
    # finder = BigramCollocationFinder.from_words(tokens)
    # trigram_measures = nltk.collocations.TrigramAssocMeasures()
    # finder.nbest(trigram_measures.raw_freq, 10)

    current_trigram = ('lose', 'yourself','in')


    for i in range(0,100):
        filter = lambda *w: current_trigram[0] != w[0] or current_trigram[1] != w[1] or current_trigram[2] != w[2]
        finder = QuadgramCollocationFinder.from_words(words)
        # only bigrams that appear 3+ times
        # finder.apply_freq_filter(3)
        # only bigrams that contain 'creature'
        finder.apply_ngram_filter(filter)
        # return the 10 n-grams with the highest PMI
        nbest = finder.nbest(quadgram_measures.likelihood_ratio, 10)
        #print(nbest[2])
        nextword = nbest[random.randint(0, len(nbest)-1)][3]
        print(nextword)
        current_trigram=(current_trigram[1], current_trigram[2], nextword)
