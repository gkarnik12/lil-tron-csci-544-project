import requests, sys, pickle
from bs4 import BeautifulSoup
from multiprocessing import Process, Pool

errors = 0
global results


def get_site_content(site):
    try:
        # start = time.time()
        response = requests.get(site, allow_redirects = True, timeout=5)
        response.raise_for_status()
        content = response.text
    except Exception as e:
        global errors
        errors += 1
        return ''
    soup = BeautifulSoup(content)
    tagwords = soup.find_all("td", class_="word")
    tagscores = soup.find_all("td", class_="score")
    words = []
    result = dict()
    w1 = site.split("/")
    w = w1[len(w1)-1]
    print(w)
  #  w = "hey"
    for i in range(0, len(tagwords)):
        word = tagwords[i].contents[1].contents[0]
        score = int(tagscores[i].contents[0])
        obj = dict()
        obj['word']=str(word)
        obj['score'] = int(str(score))
        words.append(obj)
    intersection = [item for item in words if item['word'] in tokens]
    print(str(len(words))+" "+str(len(intersection)))
    result[w] = intersection
    return result

sites = []
tokens = []
url = "http://www.b-rhymes.com/rhyme/word/"
with open(sys.argv[1], 'rb') as handle:
    tokens = pickle.load(handle)
    for token in tokens:
        sites.append(url+token)

pool = Pool(processes=10)
results = pool.map(get_site_content, sites)
with open(sys.argv[2], 'wb') as handle:
    pickle.dump(results, handle)
print(results)
print('\n\n\n\n\n\n')