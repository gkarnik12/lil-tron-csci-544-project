__author__ = 'gandhali'

import pickle
import json


def normalize(d, name):
    for key in d:
        rhymelist = d[key]
        if len(rhymelist) == 0:
            continue
        min = rhymelist[len(rhymelist)-1]['score']
        max = rhymelist[0]['score']
        for i, w in enumerate(rhymelist):
            w_new = dict()
            if(max-min) <= 0:
                w_new['score'] = 1
            else:
                w_new['score'] = w['score']/max
            w_new['word'] = w['word']
            rhymelist[i] = w_new
        d[key] = rhymelist
    with open(name, 'w') as h:
        json.dump(d, h)
    return d




brhymelist = []
brhymes = dict()
rhymebrain = dict()

with open("brhyme.txt", 'rb') as handle:
    brhymelist = pickle.load(handle)
   # print(brhymelist)

with open("rhymebrain.txt", 'rb') as handle:
    rhymebrain = pickle.load(handle)

for entry in brhymelist:
    if type(entry) == dict:
        key = list(entry.keys())[0]
        brhymes[key] = entry[key]


brhymes = normalize(brhymes, "a")
rhymebrain = normalize(rhymebrain, "b")

new_dict = dict()
for key in rhymebrain:

        if key not in brhymes or (key in brhymes and len(brhymes[key]) == 0):
            new_dict[key] = rhymebrain[key]
            continue
        a = brhymes[key]
        b = rhymebrain[key]
        c_dict = dict()
        c = []
        i = 0
        j = 0

        while i < len(a) and j < len(b):
            if a[i]['score'] >= b[j]['score']:
                c_dict[a[i]['word']] = a[i]['score']
                i += 1
            else:
                c_dict[b[j]['word']] = b[j]['score']
                j += 1
        if i <= len(a)-1:
            for obj in a[i:len(a)-1]:
                if obj['word'] not in c_dict:
                    c_dict[obj['word']] = obj['score']
        elif j <= len(b)-1:
            for obj in b[j:len(a)-1]:
                if obj['word'] not in c_dict:
                    c_dict[obj['word']] = obj['score']
        for k in c_dict:
            temp = dict()
            temp['word'] = k
            temp['score'] = c_dict[k]
            c.append(temp)
        c.sort(key=lambda item: item['score'], reverse=True)
        new_dict[key] = c

with open("rhymes_normalized", 'wb') as h:
        pickle.dump(new_dict, h)
print("That's all folks!!")
