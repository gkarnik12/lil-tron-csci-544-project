import sys, requests, json, os, pickle, urllib
from bs4 import BeautifulSoup as Soup
from urllib.parse import quote
from htmldom import htmldom

ip = open(sys.argv[1], 'rb')
op = open(sys.argv[2],'wb')
url = "http://rhymebrain.com/talk?function=getRhymes&word="
words=pickle.load(ip)
allwords={}
fetched={}

count=0
for word in words:
    word=word.lower()
    
    if len(word)>50 or word in fetched:
        continue
    fetched[word]=True
    req=url+urllib.parse.quote(word)
    print(req+" "+str(count))
    count+=1
    response = requests.get(req)
    html = response.text
    jhtml=json.loads(html)

    intersection = [item for item in jhtml if item['word'] in words]
    print(str(len(jhtml))+" "+str(len(intersection)))
    allwords[word]=intersection
pickle.dump(allwords, op)
op.close()
ip.close()
