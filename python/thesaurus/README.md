#Thesaurus

##Installation
- Two libraries which need to be installed (`lxml`, `requests`) for html parsing and grabbing webpages

- To install:

		pip3 install lxml
		pip3 install requests

##Overview

- Use `scraper.py` to grab the related word info on a list of words

###`scraper.py`

- Use this one! No API key required :)
- Example use:

		# -n is how many processes/workers you want to divide the work amongst
		python3 scraper.py -i pickled_set_file -o related_words_output_file -s slang_output_file -n 70

- This script grabs html from [http://words.bighugelabs.com](http://words.bighugelabs.com)
- We grab the html page for each word, then parse it to store **related words**, **synonyms**, **antonyms**, etc.


####Output of `scraper.py`

- Related words file, pickled array

		# format
		[(word, dictionary), (word2, dictionary2), (word3, dictionary3), ... (wordN, dictionaryN)]

		# word -> type(str)
		# Some keys in dictionary may not exist, make sure to use the .get() method for safety
		# dictionary -> {
            'noun' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'verb' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'adverb' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'adjective' : {
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'sounds_like' : {
                'syn' : [words] 
            },
            'rhymes_with' : {
                'syn' : [words]
            },
            'user_rel' : {
                'syn' : [words]
            },
            'user_rhymes_with' : {
                'syn' : [words]
            }
        }
- Slang words

		[word1, word2, word3, ... wordN]
		# word -> type(str)



- Detailed use


		usage: scraper.py [-h] [-w WORD | -i INPUT_FILE] [-n NUM_PROCESSES]
		                  [-o OUTPUT_FILE] [-s SLANG_FILE]
		
		Scrapes the bighugelabs html webpages for thesaurus information
		
		optional arguments:
		  -h, --help            show this help message and exit
		  -w WORD, --word WORD  A word to look for
		  -i INPUT_FILE, --input_file INPUT_FILE
		                        A file containing a pickled set of words
		  -n NUM_PROCESSES, --num_processes NUM_PROCESSES
		                        The number of different processes for map reduce
		  -o OUTPUT_FILE, --output_file OUTPUT_FILE
		                        A file containing a pickled array of related word
		                        objects
		  -s SLANG_FILE, --slang_file SLANG_FILE
		                        A file which contains an array of "slang" words or
		                        unfound words from the thesaurus
	
	                        
###`thesaurus.py`

- ***Note:*** This script requires an API key(s) to fetch data
- This code uses a RESTful approach
- It queries the bighuge labs API [http://words.bighugelabs.com/api](http://words.bighugelabs.com/api) for JSON

		usage: thesaurus.py [-h] pickle_file output_file
		
		A program which takes a Vocabulary and finds related words via the Big Huge
		Labs API
		
		positional arguments:
		  pickle_file  A vocabulary in the form of a pickled set
		  output_file  A file which contains all related words
		
		optional arguments:
		  -h, --help   show this help message and exit