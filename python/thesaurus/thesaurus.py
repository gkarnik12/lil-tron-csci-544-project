__author__ = 'Nii Mante'

import requests
import argparse
import pickle
import json
from multiprocessing import Pool
import sys


def create_parser():

    parser = argparse.ArgumentParser(
            description='A program which takes a Vocabulary and finds related words via the Big Huge Labs API')
    parser.add_argument(
            'pickle_file',
            type=argparse.FileType('rb'),
            help='A vocabulary in the form of a pickled set')
    parser.add_argument(
            'output_file',
            type=argparse.FileType('wb'),
            help='A file which contains all related words')

    return parser

def _big_huge_pos_tag_for_wordnet_pos_tag(pos_tag):
    """Convert the given WordNet style PoS Tag into a Big Huge Thesaurus Style PoS Tag
        Keyword arguments:
        - `pos_tag` -- The WordNet style PoS Tag (**mandatory**, *no default*)
    """
    tag = 'noun'

    if (pos_tag == 'v'):
        tag = 'verb'
    elif (pos_tag == 'a' or pos_tag == 'r' or pos_tag == 's'): # a=adjective, r=adverb, s=satellite adjective
        tag = 'adjective'

    return tag

def thesaurus_entry_raw(word, api_key, response_format='json', return_complete_response_obj=False):
    """Return the raw Thesaurus entry for the given word (in the given response format)
        Keyword arguments:
        - `word` -- The word to look up (**mandatory**, *no default*)
        - `api_key` -- Your API Key for the Big Huge Thesaurus, get your key here: https://words.bighugelabs.com/getkey.php (**mandatory**, *no default*)
        - `response_format` -- Desired format of the response, possible options are `json`, `xml` and `php`, see https://words.bighugelabs.com/api.php for more details. NB: `json` is returned as `dict`, `xml` and `php` are returned as `str` (*default:* `json`)
        - `return_complete_response_obj` -- Returns the full python-requests (http://docs.python-requests.org/en/latest/) response object (*default:* `False`)
    """

    # Specify response format
    if (response_format != None):

        # Quick Sanity Check
        if (not response_format.lower() in ['xml', 'json', 'php']):
            response_format = 'json'
    else:
        response_format = 'json'

    # Create Request URL
    url = 'http://words.bighugelabs.com/api/2/{}/{}/{}'.format(api_key, word, response_format)

    r = requests.get(url)
    #if r.status_code >= 400:
    #    raise Exception

    # Raise if the request failed
    r.raise_for_status()

    # If format is not json, then the content is returned, to be parsed by the client
    return r if return_complete_response_obj else (r.json() if response_format == 'json' else r.content)

def thesaurus_entry(word, api_key, pos_tag, ngram=1, relationship_type=None):
    """Return the Thesaurus entry for the given word.
        Keyword arguments:
        - `word` -- The word to look up (**mandatory**, *no default*)
        - `api_key` -- Your API Key for the Big Huge Thesaurus, get your key here: https://words.bighugelabs.com/getkey.php (**mandatory**, *no default*)
        - `pos_tag` -- WordNet style (i.e. `'n'`, `'v'`, `nltk.corpus.wordnet.NOUN`) or Big Huge Thesaurus style PoS Tag (i.e. `'noun'`, `'verb'`), use the function `thesaurus_entry_raw` if you don't want a PoS filter (**mandatory**, *no default*)
        - `ngram` -- Filter for specific n-grams, pass `ngram=0` to get all n-grams (*default:* `0`)
        - `relationship_type` -- Use `'syn'` for synonyms, `'ant'` for antonyms, `'rel'` for related terms, `'sim'` for similar terms, `'usr'` for user suggestions and `None` for all (*default:* `None`)
    """

    # Map WordNet PoSTag to BigHuge PoSTag (if it is a WordNet PoS Tag)
    if (pos_tag != None):
        pos_tag = _big_huge_pos_tag_for_wordnet_pos_tag(pos_tag) if len(pos_tag) == 1 else pos_tag

    # Result is a dict of dicts
    result = thesaurus_entry_raw(word=word, api_key=api_key, response_format='json')
    #result = result.get(pos_tag, {'fail': []})

    # Fiddle out the given relationship_type entry
    if (relationship_type != None):

        result_list = result.get(relationship_type, [])

        if (len(result_list) > 0):
            # Filter by ngram
            if (ngram > 0):
                return [w for w in result_list if w.count(' ') == (ngram - 1)]
            else:
                return [w for w in result_list]

    return result 

#def fetch(inputPickleFile, outputFile):
def fetch(apiWordsTuples):
    relatedWords = [] 
    words = apiWordsTuples[1]
    apiKey = apiWordsTuples[0]
    for word in words:
        try:
            jsonResponse = thesaurus_entry(word=word, api_key=apiKey, pos_tag=None)
            relatedWords.append((word, jsonResponse))
        except Exception:
            print(word)
            print('"' + word +'"' + ' not found', file=sys.stderr)
    
    #pickle.dump(relatedWords, outputFile)
    return relatedWords 

def divide(mList, numChunks):
    """
    Divides a list into n chunks

    params:
        mList -- The list to divide into chunks
        numChunks -- The number of lists to return

    returns:
        A list of lists. Each list contains >= 1 or <= len(mList)/numChunks elements
    """
    if numChunks == 0:
        return [mList]
    lists = []
    chunkLength = len(mList)//numChunks
    for i in range(numChunks):
        begin, end = i*chunkLength, (i+1)*chunkLength
        chunk = mList[begin:end]
        lists.append(chunk)
    return lists

def grab_words(words, numWordsPerChunk=1000):
    # Break word list into chunk of lists
    numChunks = len(words)//numWordsPerChunk
    wordLists = divide(words, numChunks)
    
    # create a list of tuples
    # each tuple is an (apiKey, chunkWordList) pair
    newList = []
    keys = api_keys()['big_huge_labs'][1]
    for index, wordChunkList in enumerate(wordLists):
        if index < len(keys):
            newList.append((keys[index], wordChunkList))
    # Length of each chunk will be num api requests for that day (1000)
    # Create a thread for each chunk
    #pool = Pool(processes=numChunks)
    # Map our fetch function to the chunk of words
    # get back a chunk of json objects for each chunk of words (Nwords -> Njson)
    chunkedJsonApiResponses = [] 
    for key, mList in newList:
        chunkedJsonApiResponses.append(fetch((key,mList)))

    #chunkedJsonApiResponses = pool.map(fetch, newList)
    # Reduce each chunk of json objects (list) to a total list
    allItems = [item for subList in chunkedJsonApiResponses for item in subList]
    
    return allItems


def api_keys():
    return {
            'big_huge_labs' : ( 'http://words.bighugelabs.com/', [                
                'c8ae38b73d7cee8128f30ef03a22e219',
                '0369a6789bfa07dbbbf11aceec3e262d',
                '387dfb180cb0c981d84d07f0b0da3291',
                '2f5c83e3c2ef0e7d7a3684fc8acb26d6',
                '91155e4ef3008aa077a7146c70498bba',
                '211102c2f165f3a918dfac09b34395c2',
                '265350083100a65fe0664ec8814f3730',
                '59d469efa7ccb9d6f7be4afdb5365d8d',
                '2d68ebe5bed530c8f588ca1a3c3422d4',
                '932af1d11da76102b929dd572b69c8f5',
                'b7fd6b4dbb00afcd027f1b88b0cb769a',
                '581735e8096421810cdbdacc051c557f',
                '5b5f643bc7f412de8a82db89fbf994f5',
                '3d30a447371c2988f75c00369c66cfba',
                '49e25890ad25fa0ec7f84ac53723c870',
                '77e31ace80a30967ccfd7cc902ae7e67',
                '1be6a7f14f17d98e7994327ad1e331e0',
                'dda2131ecf8682c96ff1302973beaf79',
                'f65687b67a549a4476375367f6d424b0',
                '0aa2b882749323cb6039c53620306d65',
                'bc6c9a2530f62f1463b77c8cb9311328',
                'dd229ce28f5607848ee16a2062aa8405'

                ]) 

    }

def main():
    parser = create_parser()
    args = parser.parse_args()
    words = pickle.load(args.pickle_file)
    #fetch(args.pickle_file, args.output_file) 
    thesaurus = grab_words(list(words), numWordsPerChunk=1000) 
    
    print(thesaurus, file=sys.stderr)
    pickle.dump(thesaurus, args.output_file)

if __name__ == "__main__":
    main()
