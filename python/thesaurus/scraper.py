__author__ = 'Nii Mante'

import argparse
import requests
from lxml import html
import sys
import pickle
from multiprocessing import Pool

def create_parser():
    parser = argparse.ArgumentParser(description='Scrapes the bighugelabs html webpages for thesaurus information') 
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
            '-w',
            '--word',
            help='A word to look for')
    group.add_argument(
            '-i',
            '--input_file',
            type=argparse.FileType('rb'),
            help='A file containing a pickled set of words')
    parser.add_argument(
            '-n',
            '--num_processes',
            type=int,
            default=1,
            help='The number of different processes for map reduce')
    parser.add_argument(
            '-o',
            '--output_file',
            type=argparse.FileType('wb'),
            help='A file containing a pickled array of related word objects')
    parser.add_argument(
            '-s',
            '--slang_file',
            type=argparse.FileType('wb'),
            help='A file which contains an array of "slang" words or unfound words from the thesaurus')
    
    return parser

def divide(mList, numChunks):
    """
    Divides a list into n chunks

    params:
        mList -- The list to divide into chunks
        numChunks -- The number of lists to return

    returns:
        A list of lists. Each list contains >= 1 or <= len(mList)/numChunks elements
    """
    if numChunks < 2 or numChunks > len(mList):
        return [mList]
    chunkLength = len(mList)//numChunks
    lists = \
            [mList[i*len(mList)//numChunks:(i+1)*len(mList)//numChunks] \
            for i in range(len(mList)) if i % len(mList)//numChunks == 0]
    return lists

def get_content(word):
    """
    Grabs an html webpage for a specific word from bighugelabs thesaurus website

    params:
        word -- The word we want to find related words, synonyms, similar terms 
                etc. for
    
    returns:
        A json object containing related verbs, adjectives, nouns, and more parts 
        of speech. It has this structure:
        {
            'noun' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'verb' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'adverb' : { 
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'adjective' : {
                'syn' : [words], 
                'ant' : [words], 
                'rel' : [words], 
                'sim' : [words]
            },
            'sounds_like' : {
                'syn' : [words] 
            },
            'rhymes_with' : {
                'syn' : [words]
            },
            'user_rel' : {
                'syn' : [words]
            },
            'user_rhymes_with' : {
                'syn' : [words]
            }
        }
    """

    # We'll just look at bighugelabs for now
    url='http://words.bighugelabs.com/{}'
    wordURL = url.format(word)

    # Set up fault tolerance, so that if a request fails we can try one more time
    session = requests.Session()
    session.mount("http://", requests.adapters.HTTPAdapter(max_retries=1))
    session.mount("https://", requests.adapters.HTTPAdapter(max_retries=1))

    # Wrap this in a try except in case the request fails too many times
    # or if we timeout
    try:
        page = session.get(wordURL, timeout=5)
    except Exception:
        return {} 

    # Create an xpath tree from the html text
    # we got from the thesaurus website
    tree = html.fromstring(page.text)

    # If we get back text like 'Word not found' or "I couldn't find that"
    # That means the word is not in the thesaurus
    # So we should just return an empty object
    text = tree.xpath('//*[@id="results"]/text') 
    if type(text) is list and len(text) > 0:
        return {}
    errResponse = tree.xpath('//*[@id="results"]/h3[1]/text()')
    if type(errResponse) is list and len(errResponse) > 0 and "I couldn't" in errResponse[0]:
        return {}

    child_elem = tree.xpath('//*[@id="results"]/*')
    
    # The html we get back has header text which correlates to the words following
    # For example, one section could contain a "Sounds kind of like" set of words
    # We'll use this dictionary to map the header text to one word keys
    header_map = { 
            'antonyms' : 'ant', 
            'sounds kind of like' : 'sounds_like',
            'rhymes with' : 'rhymes_with',
            'verb' : 'verb',
            'noun' : 'noun',
            'adverb' : 'adverb',
            'adjective' : 'adjective',
            'similar terms' : 'sim',
            'related terms' : 'rel',
            'user suggested related terms' : 'user_rel',
            'user suggested rhymes': 'user_rhymes_with'
    } 
    content = {}
    for ind, elem in enumerate(child_elem):
        if 'h3' in elem.tag:
            # element after us can be an h4 or ul
            POSkey = header_map[child_elem[ind].text]
            words = child_elem[ind].xpath('following::ul[1]/li/a/text()')
            if POSkey not in content:
                content[POSkey] = {}
            if child_elem[ind+1].tag in 'h4':
                nestedKey = header_map[child_elem[ind+1].text] 
                content[POSkey][nestedKey] = words 
            elif child_elem[ind+1].tag in 'ul':
                content[POSkey]['syn'] = words
        elif 'h4' in elem.tag:
            # when we see an h4 tag, we know the next item will be an ul/list of words
            words = elem.xpath('following::ul[1]/li/a/text()')
            POSkey = elem.xpath('preceding::h3')[-1].text
            nestedKey = header_map[elem.text]
            content[POSkey][nestedKey] = words
    return content

def scrape_html(words):
    url='http://words.bighugelabs.com/{}'
    related_words = []
    slang_words = []
    
    for word in words:
        wordJson = get_content(word)
        if wordJson == {}:
            print(word + ' not found.', file=sys.stderr)
            slang_words.append(word)
        else:
            related_words.append((word, wordJson)) 
    return related_words, slang_words

theRelatedWords, theSlangWords = [],[]
def createResults(result):
    """
    Callback function for our asynchronous mapping function (map_async)

    params:
        result -- When the asynchronous fetching is done, the results from each
                  process are returned as an array of results
    
    """
    
    """
        - result is a list of lists
        - Each sub list is an array of tuples
        - the first item in each tuple is an array of related words
        - The second item in each tuple is a list of slang words
        - This loop looks at each sublist, and combines all related words
        - into one array, and all slang words into one array

    result = [ [ (r0,s0), (r2, s2), (r3,s3), ... ], [ (r0,s0), (r2, s2), (r3,s3), ... ], ... ] 
    # each rN or sN is an array
    relatedWords = [ r0, r1, r2, ... rN ]
    slangWords = [ s0, s1, s2, ... sN ] 
    """
    for related_word_arr, slang_word_arr in result:
        theRelatedWords.extend(related_word_arr)
        theSlangWords.extend(slang_word_arr)

def main():
    parser = create_parser()
    args = parser.parse_args()
    words = []
    if args.word:
        # Grab one word from the command line
        words = [args.word]
    elif args.input_file:
        # Find the thesaurus info for a list of words in a file
        words = list(pickle.load(args.input_file))
    else:
        # Just try a predefined set of words
        words = ['go', 'ill', 'illusion', 'bite','hello', 'table', 'what', 'goodbye', 'ljkaf']
        print('Using example words: {}'.format(words), file=sys.stderr)
    
    # Create a pool of processes to divide the work
    pool = Pool(processes=args.num_processes) 

    # Divide the words into smaller chunks/arrays
    chunks = divide(words,args.num_processes)

    # Tell all the worker processes to scrape html for a chunk of words
    pool.map_async(scrape_html, chunks, callback=createResults)
    pool.close()
    pool.join()
    
    # theRelatedWords and theSlangWords are set in the callback=createResults function
    # relatedWords = [(word, dictionary1), (word2, dictionary2), ... (wordN, dictionaryN)]
    # slangWords = [word, word1, word2, ...wordN]

    relatedWords, slangWords = theRelatedWords, theSlangWords 

    if args.output_file:
        # Write the related words to an output file
        pickle.dump(relatedWords, args.output_file)
    else:
        # Print to stdout, can use redirect > if you want
        print(relatedWords)

    if args.slang_file:
        # Write the slang words to an output file
        pickle.dump(slangWords, args.slang_file)
    else:
        print('>>>>>>>> SLANG >>>>>>>>')
        print(slangWords)

        import time, datetime
        ts = time.time()
        dateStamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M-%S') 
        with open('slang_file_{}'.format(dateStamp), 'wb') as slang_file:
            pickle.dump(slangWords, slang_file)
        
if __name__ == "__main__":
    main()
